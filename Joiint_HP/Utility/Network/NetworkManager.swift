//
//  NetworkManager.swift
//  Joiint_HP
//
//  Created by harish on 23/08/21.
//

import Foundation

struct NetworkManager {
    static let shared = NetworkManager()
//    let session: URLSession
    
    private init() {
//        session = URLSession.shared
    }
    
    func getCitiesWeather(_ url: URL, completion: @escaping ((_ citiesWeather: [RawWeather]?, _ error: Error?) -> Void)) {
        URLSession.shared.dataTask(with: url) { data, uRLResponse, error in
            guard error == nil else {
                debugPrint(error!.localizedDescription)
                completion(nil, error)
                return
            }
            var weather: [RawWeather]?
            
            if let rData = data {
                let decoder = JSONDecoder()
                do {
                    let responseData = try decoder.decode(ResponseData.self, from: rData)
                    weather = responseData.list
                } catch let err {
                    debugPrint(err.localizedDescription)
                    completion(nil, err)
                    return
                }
            }
            completion(weather, nil)
            
        }.resume()
    }
}
