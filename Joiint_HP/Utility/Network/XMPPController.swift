//
//  XMPPController.swift
//  Joiint_HP
//
//  Created by harish on 23/08/21.
//

import Foundation
import XMPPFramework

//let host = "stun.joiint.com"
let host = "hell.la"
let port: UInt16 = 5222


enum XMPPControllerError: Error {
    case wrongUserJID
}

protocol XMPPDelegate {
    func xMPPDidConnect(_ sender: XMPPController)
    func xMPPDidDisconnect(_ sender: XMPPController)
}
class XMPPController: NSObject {
    var xmppStream: XMPPStream
    var delegate: XMPPDelegate?
    
    let hostName: String
    let userJID: XMPPJID
    let hostPort: UInt16
    let password: String

    init(hostName: String, userJIDString: String, hostPort: UInt16 = 5222, password: String) throws {
        guard let userJID = XMPPJID(string: userJIDString) else {
            throw XMPPControllerError.wrongUserJID
        }
        
        self.hostName = hostName
        self.userJID = userJID
        self.hostPort = hostPort
        self.password = password
        
        // Stream Configuration
    
        self.xmppStream = XMPPStream()
        self.xmppStream.hostName = hostName
        self.xmppStream.hostPort = hostPort
//        self.xmppStream.startTLSPolicy = .preferred
        self.xmppStream.myJID = userJID
        
        super.init()
        
        self.xmppStream.addDelegate(self, delegateQueue: DispatchQueue.main)
    }
    
    func connect() {
        if !self.xmppStream.isDisconnected {
            return
        }

        try! self.xmppStream.connect(withTimeout: 10.0)
    }
    
    func disconnet() {
        if self.xmppStream.isConnected == true {
            self.xmppStream.disconnectAfterSending()
        }
    }
}

extension XMPPController: XMPPStreamDelegate {
    func xmppStream(_ sender: XMPPStream, didNotRegister error: DDXMLElement) {
        debugPrint(#function, error)
    }
    func xmppStream(_ sender: XMPPStream, didReceiveError error: DDXMLElement) {
        debugPrint(#function, error)
    }
    func xmppStreamConnectDidTimeout(_ sender: XMPPStream) {
        debugPrint(#function)
    }
    func xmppStream(_ sender: XMPPStream, socketDidConnect socket: GCDAsyncSocket) {
        debugPrint(#function)
    }
    func xmppStreamDidConnect(_ stream: XMPPStream) {
        debugPrint("Stream: Connected")
        try! stream.authenticate(withPassword: self.password)
        self.delegate?.xMPPDidConnect(self)
    }
    
    func xmppStreamDidAuthenticate(_ sender: XMPPStream) {
        self.xmppStream.send(XMPPPresence())
        debugPrint("Stream: Authenticated")
    }
    
    func xmppStream(_ sender: XMPPStream, didNotAuthenticate error: DDXMLElement) {
        debugPrint("Stream: Fail to Authenticate")
    }
    
    func xmppStreamDidDisconnect(_ sender: XMPPStream, withError error: Error?) {
        debugPrint(#function, error?.localizedDescription ?? "NA")
    }
}
