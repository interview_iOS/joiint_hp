//
//  CodingUserInfoKey.swift
//  Joiint_HP
//
//  Created by harish on 31/08/21.
//

import Foundation

public extension CodingUserInfoKey {
  // Helper property to retrieve the Core Data managed object context
  static let managedObjectContext = CodingUserInfoKey(rawValue: "managedObjectContext")
}
