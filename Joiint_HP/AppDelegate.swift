//
//  AppDelegate.swift
//  Joiint_HP
//
//  Created by harish on 22/08/21.
//
//https://blog.pusher.com/mvvm-ios/
//https://www.erlang-solutions.com/blog/build-a-complete-ios-messaging-app-using-xmppframework-part-2/
import UIKit
import CoreData
import CocoaLumberjack

@main
class AppDelegate: UIResponder, UIApplicationDelegate {


    var window: UIWindow?
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
//        if let logShared = DDTTYLogger.sharedInstance {
            DDLog.add(DDOSLogger.sharedInstance, with: .debug)
//        }
        
        CoreDataManager.shared.persistentContainer.loadPersistentStores { storeDescription, error in
            CoreDataManager.shared.persistentContainer.viewContext.mergePolicy = NSMergeByPropertyObjectTrumpMergePolicy

            if let error = error {
                print("Unresolved error \(error)")
            }
        }
        return true
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        CoreDataManager.shared.saveContext()
        let paths = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)
        print("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@")
        print(paths[0])
    }

}

