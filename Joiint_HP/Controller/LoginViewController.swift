//
//  LoginViewController.swift
//  Joiint_HP
//
//  Created by harish on 23/08/21.
//

import UIKit

class LoginViewController: UIViewController {

    var xMPPCon: XMPPController?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func btnUser0(_ sender: Any) {
        if xMPPCon?.xmppStream.isConnected == true {
            self.performSegue(withIdentifier: "WeatherSegID", sender: nil)
        }
        else {
            self.connect(with: "happysingh@hell.la")
        }
//        self.connect(with: "joiint-1@stun.joiint.com")
    }
    
    @IBAction func btnDisconnect(_ sender: Any) {
        xMPPCon?.disconnet()
    }
    
    private func connect(with jid: String) {
        do {
            try xMPPCon = XMPPController(hostName: host, userJIDString: jid, password: "Happy$ingh")
            xMPPCon?.delegate = self
        } catch let err {
            debugPrint(#function, err.localizedDescription)
        }
        
        xMPPCon?.connect()
    }
}
extension LoginViewController: XMPPDelegate {
    func xMPPDidConnect(_ sender: XMPPController) {
        self.performSegue(withIdentifier: "WeatherSegID", sender: nil)
    }
    
    func xMPPDidDisconnect(_ sender: XMPPController) {
    }
}
