//
//  DetailViewController.swift
//  Joiint_HP
//
//  Created by harish on 24/08/21.
//

import UIKit

class DetailViewController: UIViewController {

    @IBOutlet weak var tableViewCities: UITableView!
    @IBOutlet weak var imgView: UIImageView!
    var weatherVM: WeatherViewModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        if let url = weatherVM?.getIconURL() {
            var img: UIImage?
            DispatchQueue.global(qos: .background).async {
                do {
                    let data = try Data(contentsOf: url, options: .mappedIfSafe)
                    img = UIImage(data: data)
                } catch let err {
                    debugPrint(err.localizedDescription)
                }
                DispatchQueue.main.async {
                    self.imgView.image = img
                }
            }
        }
    }
}
extension DetailViewController: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 3
        } else if section == 1 {
            return 3
        }
        else {
            return 2
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "DetailCellID") else {
            fatalError("Cell not found")
        }
        switch indexPath.section {
        case 0:
            switch indexPath.row {
            case 0:
                cell.textLabel?.text = "Description"
                cell.detailTextLabel?.text = weatherVM?.getDescription()
            case 1:
                cell.textLabel?.text = "Sunrise Time"
                cell.detailTextLabel?.text = weatherVM?.getSunriseTime()
            case 2:
                cell.textLabel?.text = "Sunset Time"
                cell.detailTextLabel?.text = weatherVM?.getSunsetTime()
            default:
                break
            }
            
        case 1:
            switch indexPath.row {
            case 0:
                cell.textLabel?.text = "Temperature"
                cell.detailTextLabel?.text = weatherVM?.getTemprature()
            case 1:
                cell.textLabel?.text = "Pressure"
                cell.detailTextLabel?.text = weatherVM?.getPressure()
            case 2:
                cell.textLabel?.text = "Humidity"
                cell.detailTextLabel?.text = weatherVM?.getHumidity()
            default:
                break
            }
            
        case 2:
            switch indexPath.row {
            case 0:
                cell.textLabel?.text = "Wind Speed"
                cell.detailTextLabel?.text = weatherVM?.getWindSpeed()
            case 1:
                cell.textLabel?.text = "Wind Degree"
                cell.detailTextLabel?.text = weatherVM?.getWindDegree()
            default:
                break
            }
        
        default:
            break
        }
        return cell
    }
    
    //MARK:- UITableViewDelegate
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 0 {
            return weatherVM?.getCity_Country()
        }
        return nil
    }
    func tableView(_ tableView: UITableView, titleForFooterInSection section: Int) -> String? {
        if section == 2 {
            return weatherVM?.getLastUpdate()
        }
        return nil
    }
}
