//
//  CitiesViewController.swift
//  Joiint_HP
//
//  Created by harish on 23/08/21.
//

import UIKit

class CitiesViewController: UIViewController {

    @IBOutlet weak var tableViewCities: UITableView!
    
    var weatherViewModel: CitiesWeatherViewModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.fetchNearByCitiesWeather()
    }
    

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        if let detailVC = segue.destination as? DetailViewController,
           let index = (sender as? UITableViewCell)?.tag {
            detailVC.weatherVM = weatherViewModel?.getCityWeather(at: index)
        }
    }

    
    /// Create URL and make request to fetch data from clound
    func fetchNearByCitiesWeather() {
        guard let uRL = URL(string: "https://api.openweathermap.org/data/2.5/find?lat=21.171786&lon=72.831681&cnt=20&appid=49a47448d82050990d49e95bec10a722") else {
            return
        }
        NetworkManager.shared.getCitiesWeather(uRL) { data, error in
            guard let rawWeathers = data else { return }
            self.weatherViewModel = CitiesWeatherViewModel(rawWeathers)
            DispatchQueue.main.async {
                self.tableViewCities.reloadData()
            }
            debugPrint("rawWeathers: ", rawWeathers.count)
        }
    }
}

extension CitiesViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return weatherViewModel?.numberOfCities ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "CityCellId") else {
            fatalError("Cell not found")
        }
        if let cityWeather = weatherViewModel?.getCityWeather(at: indexPath.row) {
            cell.imageView?.image = UIImage(named: "10d")
            cell.textLabel?.text = cityWeather.getCity_Country()
            cell.detailTextLabel?.text = cityWeather.getTemprature()
        }
        cell.tag = indexPath.row
        return cell
    }
    
    //MARK: - UITableViewDelegate
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
