//
//  WeatherViewModel.swift
//  Joiint_HP
//
//  Created by harish on 23/08/21.
//

import Foundation

private let mf = MeasurementFormatter()

struct WeatherViewModel {
    private let weather: Weather
    init(_ rawWeather: RawWeather) {
        
        let decoder = JSONDecoder()
        let managedObjectContext = CoreDataManager.shared.managedObjectContext()
        guard let codingUserInfoKeyManagedObjectContext = CodingUserInfoKey.managedObjectContext else {
            fatalError("Failed to retrieve managed object context Key")
        }
        decoder.userInfo[codingUserInfoKeyManagedObjectContext] = managedObjectContext
        
        do {
            let responseData = try JSONEncoder().encode(rawWeather)
            weather = try decoder.decode(Weather.self, from: responseData)

        } catch let error {
            print("decoding error: \(error)")
            weather = Weather()
        }
        
//        weather = Weather(cid: rawWeather.id,
//                          city: rawWeather.name,
//                          country: rawWeather.sys?.country,
//                          icon: rawWeather.weather?.first?.icon,
//                          descrip: rawWeather.weather?.first?.description,
//                          sunriseTime: rawWeather.sys?.sunrise,
//                          sunsetTime: rawWeather.sys?.sunset,
//
//                          temperature: rawWeather.main?.temp,
//                          pressure: rawWeather.main?.pressure,
//                          humidity: rawWeather.main?.humidity,
//
//                          windSpeed: rawWeather.wind?.speed,
//                          windDeg: rawWeather.wind?.deg,
//
//                          timeDate: rawWeather.dt)
    }
    
    func parsePersonJson(_ rW: RawWeather) {
        let responseData = Data()
        
        let decoder = JSONDecoder()
        let managedObjectContext = CoreDataManager.shared.managedObjectContext()
        guard let codingUserInfoKeyManagedObjectContext = CodingUserInfoKey.managedObjectContext else {
            fatalError("Failed to retrieve managed object context Key")
        }
        decoder.userInfo[codingUserInfoKeyManagedObjectContext] = managedObjectContext
        
        do {
            let result = try decoder.decode(Weather.self, from: responseData)
            print(result)
        } catch let error {
            print("decoding error: \(error)")
        }
        
//        CoreDataManager.shared.clearStorage(forEntity: "Person")
//        CoreDataManager.shared.saveContext()
        
//        let paths = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)
//        print("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@")
//        print(paths[0])
    }

}
extension WeatherViewModel {
    func getIconURL() -> URL? {
        guard let icon = weather.icon else { return nil }
        return URL(string: "https://openweathermap.org/img/wn/\(icon)@2x.png")
    }
    func getDescription() -> String {
        guard let description = weather.descrip else { return "-" }
        return description
    }
    func getTemprature() -> String {
        guard let temp = weather.temperature?.doubleValue else { return "-" }
        return convertTemp(temp: temp, from: .kelvin, to: .celsius)
    }
    func getPressure() -> String {
        guard let pressure = weather.pressure?.stringValue else { return "-" }
        return pressure
    }
    func getHumidity() -> String {
        guard let humidity = weather.humidity?.stringValue else { return "-" }
        return humidity
    }
    
    func getCity_Country() -> String {
        guard let city = weather.city, let country = weather.country else { return "-" }
        return city.localizedUppercase + ", " + country.localizedCapitalized
    }
    
    func getLastUpdate() -> String {
        guard let timeStamp = weather.timeDate?.intValue else { return "-" }
        let date = Date(timeIntervalSince1970: TimeInterval(timeStamp))
        return "Last updated: " + date.description
    }
    
    func getWindSpeed() -> String {
        guard let speed = weather.windSpeed?.stringValue else { return "-" }
        return speed
    }
    func getWindDegree() -> String {
        guard let windDeg = weather.windDeg?.stringValue else { return "-" }
        return windDeg
    }
    
    func getSunriseTime() -> String {
        guard let sunriseTime = weather.sunriseTime?.intValue else { return "-" }
        return self.getTime(from: sunriseTime) ?? "-"
    }
    func getSunsetTime() -> String? {
        guard let sunsetTime = weather.sunsetTime?.intValue else { return "-" }
        return self.getTime(from: sunsetTime) ?? "-"
    }
    private func getTime(from timeStamp: Int) -> String? {
        let date = Date(timeIntervalSince1970: TimeInterval(timeStamp))
        let dF = DateFormatter()
        dF.dateStyle = .none
        dF.timeStyle = .full
        return dF.string(from: date)
    }
    private func convertTemp(temp: Double, from inputTempType: UnitTemperature, to outputTempType: UnitTemperature) -> String {
        mf.numberFormatter.maximumFractionDigits = 0
        mf.unitOptions = .providedUnit
        let input = Measurement(value: temp, unit: inputTempType)
        let output = input.converted(to: outputTempType)
        return mf.string(from: output)
    }
}
