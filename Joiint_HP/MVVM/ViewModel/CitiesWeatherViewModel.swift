//
//  CitiesWeatherViewModel.swift
//  Joiint_HP
//
//  Created by harish on 30/08/21.
//

import Foundation

struct CitiesWeatherViewModel {
    private var list = [WeatherViewModel]()
    
    init(_ rawWeather: [RawWeather]) {
        self.list = getArrangedList(rawWeather)
    }
}

extension CitiesWeatherViewModel {
    var numberOfCities: Int {
        return list.count
    }
    
    /// Base on index WeatherVM will return from list if cities
    /// - Parameter index: Position from array
    /// - Returns: Return WeatherVM which has Weather model, if index exist
    func getCityWeather(at index: Int) -> WeatherViewModel? {
        guard index < self.list.count else {
            return nil
        }
        return list[index]
    }
    
    private func getArrangedList(_ data: [RawWeather]) -> [WeatherViewModel] {
        var tempList = [WeatherViewModel]()
        data.forEach {
            let tempWeather = WeatherViewModel($0)
            
            tempList.append(tempWeather)
        }
        return tempList
    }
}
