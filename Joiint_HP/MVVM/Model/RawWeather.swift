//
//  RawWeather.swift
//  Joiint_HP
//
//  Created by harish on 23/08/21.
//

import Foundation

struct ResponseData: Codable {
    var message: String?
    var cod: String?
    var count: Int?
    var list: [RawWeather]?
}
struct RawWeather: Codable {
    var id: Int?
    var name: String?
    var coord: Coordinate?
    var weather: [Weather]?
    var main: MainData?
    var wind: Wind?
    var dt: Int?
    var sys: SystemData?
    var timezone: Int?
    
    
    struct Coordinate: Codable {
        var lon: Double?
        var lat: Double?
    }
    struct Weather: Codable {
        var id: Int?
        var main: String?
        var description: String?
        var icon: String?
    }
    struct MainData: Codable {
        var temp: Double?
        //        var feels_like: Double?
        var temp_min: Double?
        var temp_max: Double?
        var pressure: Int?
        var humidity: Int?
    }
    struct Wind: Codable {
        var speed: Double?
        var deg: Int?
    }
    struct SystemData: Codable {
        var type: Int?
        var id: Int?
        var message: Double?
        var country: String?
        var sunrise: Int?
        var sunset: Int?
    }
}
