//
//  Weather.swift
//  Joiint_HP
//
//  Created by harish on 31/08/21.
//

import Foundation
import CoreData

class Weather: NSManagedObject, Codable {
    @NSManaged var cid: Int64
    @NSManaged var city: String?
    @NSManaged var country: String?
    @NSManaged var icon: String?
    @NSManaged var descrip: String?
    @NSManaged var sunriseTime: NSNumber?
    @NSManaged var sunsetTime: NSNumber?
    
    @NSManaged var temperature: NSNumber?
    @NSManaged var pressure: NSNumber?
    @NSManaged var humidity: NSNumber?
    
    @NSManaged var windSpeed: NSNumber?
    @NSManaged var windDeg: NSNumber?
    
    @NSManaged var timeDate: NSNumber?
    
    enum CodingKeys: String, CodingKey {
        case cid
        case city
        case country
        case icon
        case descrip
        case sunriseTime
        case sunsetTime
        case temperature
        case pressure
        case humidity
        case windSpeed
        case windDeg
        case timeDate
    }
    
    enum DecodingKeys: String, CodingKey {
        case cid = "id"
        case city = "name"
        case country = "country"
        case icon
        case descrip = "description"
        case sunriseTime = "sunrise"
        case sunsetTime = "sunset"
        case temperature = "temp"
        case pressure = "pressure"
        case humidity = "humidity"
        case windSpeed = "speed"
        case windDeg = "deg"
        case timeDate = "dt"
        
        case main
        case wind
        case sys
        case weather
    }
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(cid, forKey: .cid)
//        try container.encode(city, forKey: .city)
    }
    
    required convenience init(from decoder: Decoder) throws {
        guard let codingUserInfoKeyManagedObjectContext = CodingUserInfoKey.managedObjectContext,
              let managedObjectContext = decoder.userInfo[codingUserInfoKeyManagedObjectContext] as? NSManagedObjectContext,
              let entity = NSEntityDescription.entity(forEntityName: "Weather", in: managedObjectContext) else {
            fatalError("Failed to decode User")
        }
        
        self.init(entity: entity, insertInto: managedObjectContext)
        
        let container = try decoder.container(keyedBy: DecodingKeys.self)
        cid = Int64(try container.decode(Int.self, forKey: .cid))
        city = try container.decode(String.self, forKey: .city)
        if let td = try container.decodeIfPresent(Int.self, forKey: .timeDate) {
            timeDate = NSNumber(value: td)
        }
        else {
            timeDate = nil
        }
        
        let mainContainer = try container.nestedContainer(keyedBy: DecodingKeys.self, forKey: .main)
        temperature = NSNumber(value: try mainContainer.decode(Double.self, forKey: .temperature))
        if let pres = try mainContainer.decodeIfPresent(Int.self, forKey: .pressure) {
            pressure = NSNumber(value: pres)
        }
        else {
            pressure = nil
        }
        if let humid = try mainContainer.decodeIfPresent(Int.self, forKey: .humidity) {
            humidity = NSNumber(value: humid)
        }
        else {
            humidity = nil
        }
        
        let windContainer = try container.nestedContainer(keyedBy: DecodingKeys.self, forKey: .wind)
        if let speed = try windContainer.decodeIfPresent(Double.self, forKey: .windSpeed) {
            windSpeed = NSNumber(value: speed)
        }
        else {
            windSpeed = nil
        }
        if let deg = try windContainer.decodeIfPresent(Int.self, forKey: .windDeg) {
            windDeg = NSNumber(value: deg)
        }
        else {
            windDeg = nil
        }
        
        let sysContainer = try container.nestedContainer(keyedBy: DecodingKeys.self, forKey: .sys)
        country = try sysContainer.decodeIfPresent(String.self, forKey: .country)
        if let sunriseT = try sysContainer.decodeIfPresent(Int.self, forKey: .sunriseTime) {
            sunriseTime = NSNumber(value: sunriseT)
        }
        else {
            sunriseTime = nil
        }
        if let sunsetT = try sysContainer.decodeIfPresent(Int.self, forKey: .sunsetTime) {
            sunsetTime = NSNumber(value: sunsetT)
        }
        else {
            sunsetTime = nil
        }
        
        let weathers = try container.decodeIfPresent([RawWeather.Weather].self, forKey: .weather)
        weathers?.forEach {
            icon = $0.icon
            descrip = $0.description
            return
        }
        
//        let weatherContainer = try container.nestedContainer(keyedBy: DecodingKeys.self, forKey: .weather)
//        icon = try weatherContainer.decodeIfPresent(String.self, forKey: .icon)
//        descrip = try weatherContainer.decodeIfPresent(String.self, forKey: .descrip)
        
    }
}
